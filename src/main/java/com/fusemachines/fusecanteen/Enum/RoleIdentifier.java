package com.fusemachines.fusecanteen.Enum;

public enum RoleIdentifier {
    EMPLOYEE("employee"),ADMIN("admin");

    private final String value;

    RoleIdentifier(String orderStatus) {

        this.value=orderStatus;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static RoleIdentifier getEnum(String value) {
        if (value == null)
            throw new IllegalArgumentException();
        for (RoleIdentifier v : values())
            if (value.equalsIgnoreCase(v.getValue()))
                return v;
        throw new IllegalArgumentException();
    }
}
