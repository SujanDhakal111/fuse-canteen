package com.fusemachines.fusecanteen.model;

import javax.persistence.*;

@Entity
@Table(name = "foods")
public class Food {

    private Long id;
    private String name;
    private Double price;
    private Boolean isAvailableToday;

    public Food() {

    }

    public Food(String name, double price, boolean isAvailableToday) {
        this.name = name;
        this.price = price;
        this.isAvailableToday = isAvailableToday;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "price", nullable = false)
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    @Column(name = "isAvailableToday")
    public Boolean getAvailableToday() {
        return isAvailableToday;
    }
    public void setAvailableToday(Boolean isAvailableToday) {
        this.isAvailableToday = isAvailableToday;
    }

}
