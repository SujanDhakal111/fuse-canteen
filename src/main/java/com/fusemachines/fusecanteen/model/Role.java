package com.fusemachines.fusecanteen.model;

import com.fusemachines.fusecanteen.Enum.RoleIdentifier;

import javax.persistence.*;

import java.util.Set;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "identifier", nullable = false)
    private RoleIdentifier identifier;


    @Version
    private Long version;

    public Role(){

    }
    public Role(RoleIdentifier identifier){
        this.identifier = identifier;

    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public RoleIdentifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(RoleIdentifier identifier) {
        this.identifier = identifier;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
