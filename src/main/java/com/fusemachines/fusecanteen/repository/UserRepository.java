package com.fusemachines.fusecanteen.repository;

import com.fusemachines.fusecanteen.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String name);
}
