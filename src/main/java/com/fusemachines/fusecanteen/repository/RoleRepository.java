package com.fusemachines.fusecanteen.repository;


import com.fusemachines.fusecanteen.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
