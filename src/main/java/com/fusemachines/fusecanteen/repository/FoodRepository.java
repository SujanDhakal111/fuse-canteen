package com.fusemachines.fusecanteen.repository;


import com.fusemachines.fusecanteen.model.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends JpaRepository<Food, Long> {
    public List<Food> findByAvailableToday(Boolean isAvailableToday);
}
