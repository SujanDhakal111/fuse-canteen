package com.fusemachines.fusecanteen.repository;


import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.FoodRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.util.List;

@Repository
public interface FoodRequestRepository extends JpaRepository<FoodRequest, Long> {
//    @Query("select food_id, count(food_id) as popularity from food_requests group by food_id order by popularity desc")
//    public Object getFoodsByPopularity();

//    @Query("SELECT id from FoodRequest group by food")
//    public ResultSet findUniqueFoodIds();

    public List<FoodRequest> findByFoodId(Long foodId);

}
