package com.fusemachines.fusecanteen.session;

import com.fusemachines.fusecanteen.Enum.RoleIdentifier;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.User;

import javax.servlet.http.HttpSession;

public class SessionManager {

    public static User getLoggedInUser(HttpSession session) throws ResourceNotFoundException {
        User user = (User) session.getAttribute("user");
        if(user == null ) throw new ResourceNotFoundException("You cannot use this without logging in");
        return user;
    }
    public static void validateRoleOrFail(User user, RoleIdentifier expectedRole, String failMessage) throws ResourceNotFoundException{
        if (!user.getRole().getIdentifier().equals(expectedRole)) throw new ResourceNotFoundException(failMessage);
    }
}
