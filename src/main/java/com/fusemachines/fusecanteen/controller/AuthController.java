package com.fusemachines.fusecanteen.controller;


import com.fusemachines.fusecanteen.dto.LoginDto;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.User;
import com.fusemachines.fusecanteen.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class AuthController {
    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/login",consumes = {"application/json", "multipart/form-data" })
    public User loginUser(@Valid @RequestBody LoginDto loginDto,HttpSession session) throws  Exception{
        User user = userRepository.findByUsername(loginDto.getUsername());
        if(user==null) throw new Exception("Couldn't find the user");
        String storedPassword = user.retrievePassword();
        if(!storedPassword.equals( loginDto.getPassword()))
            throw new Exception("Incorrect Password");
        session.setAttribute("user",user);
        return user;
    }
}
