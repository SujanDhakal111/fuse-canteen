package com.fusemachines.fusecanteen.controller;
import com.fusemachines.fusecanteen.Enum.RoleIdentifier;
import com.fusemachines.fusecanteen.dto.FoodAvailablityBody;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.User;
import com.fusemachines.fusecanteen.repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fusemachines.fusecanteen.session.SessionManager.getLoggedInUser;
import static com.fusemachines.fusecanteen.session.SessionManager.validateRoleOrFail;

/**
 * @author  Sujan Dhakal
 * Use of service interface and its implementation for abstraction purpose is skipped due to time limitation
 */

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")
public class FoodController {
	@Autowired
	private FoodRepository foodRepository;

	@GetMapping("/food")
	public List<Food> getAllFoodDetail() {
		return foodRepository.findAll();
	}

	@GetMapping("/food/{id}")
	public ResponseEntity<Food> getFoodById(@PathVariable(value = "id") Long foodId)
			throws ResourceNotFoundException {
		Food food = foodRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFoundException("Food not found for this id :: " + foodId));
		return ResponseEntity.ok().body(food);
	}

	//Route to add food items
	@PostMapping("/food")
	public Food createFood(@Valid @RequestBody Food food, HttpSession session) throws ResourceNotFoundException {
		User requestedUser = getLoggedInUser(session);
		validateRoleOrFail(requestedUser, RoleIdentifier.ADMIN, "Only admin can create a food");
		return foodRepository.save(food);
	}

	//Route to edit food items
	@PutMapping("/food/{id}")
	public ResponseEntity<Food> updateFood(@PathVariable(value = "id") Long foodId,
										   @Valid @RequestBody Food foodDetails, HttpSession session) throws ResourceNotFoundException {
		Food food = foodRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFoundException("Food not found for this id :: " + foodId));
		User requestedUser = getLoggedInUser(session);
		validateRoleOrFail(requestedUser, RoleIdentifier.ADMIN, "Only admin can edit food");
		food.setName(foodDetails.getName());
		food.setPrice(foodDetails.getPrice());
		food.setAvailableToday(foodDetails.getAvailableToday());
		final Food updatedFood = foodRepository.save(food);
		return ResponseEntity.ok(updatedFood);
	}

	//Route to get foods available for today
	@GetMapping("/today-only-food")
	public ResponseEntity<List<Food>> todayOnlyFood() {
		List<Food> todayFoods  = foodRepository.findByAvailableToday(true);
		return ResponseEntity.ok(todayFoods);
	}

     //Route to post food available for today
	@PostMapping(path ="/today-food")
	public ResponseEntity<Food> updateAvailablity(@RequestBody FoodAvailablityBody reqBody, HttpSession session ) throws ResourceNotFoundException {
		Long foodId = reqBody.foodId;
		Food food = foodRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFoundException("Food not found for this id :: " + foodId));
		User requestedUser = getLoggedInUser(session);
		validateRoleOrFail(requestedUser, RoleIdentifier.ADMIN, "Only admin can add food available for today");
		food.setAvailableToday(reqBody.isAvailableToday);
		final Food updatedFood = foodRepository.save(food);
		return ResponseEntity.ok(updatedFood);
	}

	@DeleteMapping("/food/{id}")
	public Map<String, Boolean> deleteFood(@PathVariable(value = "id") Long foodId, HttpSession session)
			throws ResourceNotFoundException {
		Food food = foodRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFoundException("Food not found for this id :: " + foodId));
		User requestedUser = getLoggedInUser(session);
		validateRoleOrFail(requestedUser, RoleIdentifier.ADMIN, "Only admin can delete food");

		foodRepository.delete(food);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}


}
