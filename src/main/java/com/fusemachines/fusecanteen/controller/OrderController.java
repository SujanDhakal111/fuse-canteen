package com.fusemachines.fusecanteen.controller;
import com.fusemachines.fusecanteen.Enum.OrderStatus;
import com.fusemachines.fusecanteen.Enum.RoleIdentifier;
import com.fusemachines.fusecanteen.dto.OrderDto;
import com.fusemachines.fusecanteen.dto.ScheduleOrderDto;
import com.fusemachines.fusecanteen.dto.UpdateOrderStatusDto;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.Order;
import com.fusemachines.fusecanteen.model.User;
import com.fusemachines.fusecanteen.repository.FoodRepository;
import com.fusemachines.fusecanteen.repository.OrderRepository;
import com.fusemachines.fusecanteen.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

import static com.fusemachines.fusecanteen.session.SessionManager.getLoggedInUser;
import static com.fusemachines.fusecanteen.session.SessionManager.validateRoleOrFail;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FoodRepository foodRepository;

    @GetMapping("/all-employee-orders")
    public List<Order> getAllOrders(HttpSession session) throws ResourceNotFoundException {
        User user = getLoggedInUser(session);
        validateRoleOrFail(user, RoleIdentifier.ADMIN, "Only admin can view all employee orders ");
        return orderRepository.findAll();
    }

    @GetMapping("/user/my-orders")
    public List<Order> getMyOrders(HttpSession session) throws  ResourceNotFoundException{
        User user = getLoggedInUser(session);
        return orderRepository.findByUserId(user.getId());
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable(value = "id") Long orderId)
            throws ResourceNotFoundException {
        Order orders = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order not found for this id :: " + orderId));
        return ResponseEntity.ok().body(orders);
    }

    @DeleteMapping("/Order/{id}")
    public Map<String, Boolean> deleteOrder(@PathVariable(value = "id") Long OrderId)
            throws ResourceNotFoundException {
        Order Order = orderRepository.findById(OrderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order not found for this id :: " + OrderId));

        orderRepository.delete(Order);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }


    @PostMapping("/place-order")
    public Order placeOrder(@RequestBody OrderDto orderDto,HttpSession session) throws ResourceNotFoundException {
        User user = getLoggedInUser(session);
        Order order = new Order();
        List<Food> orderFoods = new ArrayList<Food>();
        orderDto.getFoodIds().forEach(foodId -> {
            try {
                Food orderFood = foodRepository.findById(foodId).orElseThrow(() -> new ResourceNotFoundException("food not found"));
                orderFoods.add(orderFood);
                order.addToTotalPrice(orderFood.getPrice());
            } catch (ResourceNotFoundException e) {
                e.printStackTrace();
            }
        });

        order.setStatus(OrderStatus.INPROCESS);
        order.setScheduledTime(orderDto.getTime());
        order.setFoods(orderFoods);
        order.setUser(user);
        order.setCreatedAt(new Date());
        return  orderRepository.save(order);
    }


    @PostMapping("/schedule-placed-order")
    public Order updateScheduledTime(@RequestBody ScheduleOrderDto scheduleOrderDto,HttpSession session) throws ResourceNotFoundException {
        User user = getLoggedInUser(session);
        validateRoleOrFail(user, RoleIdentifier.EMPLOYEE, "Only employee can schedule placed order ");
        Order order = orderRepository.findById(scheduleOrderDto.getOrderId()).orElseThrow(() -> new ResourceNotFoundException("order not found"));
        order.setScheduledTime(scheduleOrderDto.getTime());
        return  orderRepository.save(order);
    }

    @PostMapping("/update-order-status")
    public Order updateOrderStatus(@RequestBody UpdateOrderStatusDto statusDto,HttpSession session) throws ResourceNotFoundException {
        User user = getLoggedInUser(session);
        validateRoleOrFail(user, RoleIdentifier.ADMIN, "Only admin can update order status ");
        Order order = orderRepository.findById(statusDto.getOrderId()).orElseThrow(() -> new ResourceNotFoundException("order not found"));
        order.setStatus(statusDto.getOrderStatus());
        return  orderRepository.save(order);
    }



}
