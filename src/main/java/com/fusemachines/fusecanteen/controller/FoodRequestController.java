package com.fusemachines.fusecanteen.controller;
import com.fusemachines.fusecanteen.Enum.RoleIdentifier;
import com.fusemachines.fusecanteen.dto.FoodRequestBody;
import com.fusemachines.fusecanteen.dto.PopularFoodResponseDto;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.FoodRequest;
import com.fusemachines.fusecanteen.model.User;
import com.fusemachines.fusecanteen.repository.FoodRepository;
import com.fusemachines.fusecanteen.repository.FoodRequestRepository;
import com.fusemachines.fusecanteen.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.fusemachines.fusecanteen.session.SessionManager.getLoggedInUser;
import static com.fusemachines.fusecanteen.session.SessionManager.validateRoleOrFail;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")

public class FoodRequestController {
    @Autowired
    private FoodRequestRepository foodRequestRepository;

    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private UserRepository userRepository;

    //Route to view food requests
    @GetMapping("/food-requests")
    public List<FoodRequest> getAllFoodRequests() {
        return foodRequestRepository.findAll();
    }

    //Route to post food requests
    @PostMapping("/food-requests")
    public FoodRequest createRole(@Valid @RequestBody FoodRequestBody requestBody, HttpSession session)throws ResourceNotFoundException {
        FoodRequest foodRequest = new FoodRequest();
        Food requestedFood = foodRepository.findById(requestBody.foodId).
                orElseThrow(() -> new ResourceNotFoundException("Food Id not found"));
        User requestedUser = getLoggedInUser(session);
        validateRoleOrFail(requestedUser, RoleIdentifier.EMPLOYEE, "Only employee can create food requests");
        foodRequest.setDate(requestBody.date);
        foodRequest.setFood(requestedFood);
        foodRequest.setUser(requestedUser);
        return foodRequestRepository.save(foodRequest);
    }

    //Route to get food items with popularity
    @GetMapping("/popular-requests")
    public List<PopularFoodResponseDto> getPopularFoods()throws ResourceNotFoundException {
        Set<Long> uniqueFoodIds  = getDistinctFoodIdsFromFoodRequests();
        List<PopularFoodResponseDto> popularList = new ArrayList<PopularFoodResponseDto>();
        uniqueFoodIds.forEach(foodId->{
            popularList.add(mapResponseByFoodId(foodId));
        });
        return popularList;
    }

    private Set<Long> getDistinctFoodIdsFromFoodRequests(){
        List<FoodRequest> allRequests = foodRequestRepository.findAll();
        Set<Long> uniqueFoodIds  =  new HashSet<>();
        allRequests.forEach(foodRequest -> {
            uniqueFoodIds.add(foodRequest.getFood().getId());
        });
        return  uniqueFoodIds;
    }

    private PopularFoodResponseDto mapResponseByFoodId(Long foodId){
        PopularFoodResponseDto popularDto = new PopularFoodResponseDto();
        List<FoodRequest> requestsByFood = foodRequestRepository.findByFoodId(foodId);
        requestsByFood.forEach(req->{
            popularDto.addRequestedUser(req.getUser());
            popularDto.setFood(req.getFood());
        });
        return popularDto;
    }
}
