package com.fusemachines.fusecanteen.controller;
import com.fusemachines.fusecanteen.model.Role;
import com.fusemachines.fusecanteen.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")

public class RoleController {
    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/role")
    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

    @PostMapping("/create-role")
    public Role createRole(@Valid @RequestBody Role role) {
        return roleRepository.save(role);
    }
}
