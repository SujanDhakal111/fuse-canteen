package com.fusemachines.fusecanteen.controller;
import com.fusemachines.fusecanteen.exception.ResourceNotFoundException;
import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.Role;
import com.fusemachines.fusecanteen.model.User;
import com.fusemachines.fusecanteen.repository.RoleRepository;
import com.fusemachines.fusecanteen.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1")

public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping("/user")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @PostMapping(value = "/create-user",consumes = {"application/json", "multipart/form-data" })
    public User createUser(@Valid @RequestBody UserRequest request) throws ResourceNotFoundException {
        User user = new User();
        Long roleId = request.roleId;
        Role requestRole = roleRepository.findById(roleId).
                orElseThrow(() -> new ResourceNotFoundException("role not found for this id :: " + roleId));
        user.setRole(requestRole);
        user.setPassword(request.password);
        user.setUsername(request.username);
        return userRepository.save(user);
    }
}
