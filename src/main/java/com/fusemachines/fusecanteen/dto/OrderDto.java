package com.fusemachines.fusecanteen.dto;

import com.fusemachines.fusecanteen.Enum.OrderStatus;
import com.fusemachines.fusecanteen.model.Order;
import lombok.Data;

import java.util.Date;
import java.util.List;


public class OrderDto {
    private List<Long> foodIds;
    private String time;

    public List<Long> getFoodIds() {
        return foodIds;
    }

    public void setFoodIds(List<Long> foodIds) {
        this.foodIds = foodIds;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
