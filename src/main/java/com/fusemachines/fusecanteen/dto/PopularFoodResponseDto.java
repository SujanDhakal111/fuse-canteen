package com.fusemachines.fusecanteen.dto;

import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.User;

import java.util.ArrayList;
import java.util.List;

public class PopularFoodResponseDto {
    private Food food;
    private int requestCount = 0;
    private List<User> requestedUsers = new ArrayList<User>();

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public int getRequestCount() {
        return requestCount;
    }

    public void setRequestCount(int requestCount) {
        this.requestCount = requestCount;
    }

    public List<User> getRequestedUsers() {
        return requestedUsers;
    }

    public void setRequestedUsers(List<User> requestedUsers) {
        this.requestedUsers = requestedUsers;
    }

    public void addRequestedUser(User requestedUser) {
        this.requestCount++;
        this.requestedUsers.add(requestedUser);
    }

}
