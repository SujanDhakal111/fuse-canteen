package com.fusemachines.fusecanteen.dto;

import java.util.Date;

public class FoodRequestBody {
    public Long foodId;
    public Date date;

    public Long getFoodId() {
        return foodId;
    }

    public void setFoodId(Long foodId) {
        this.foodId = foodId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
