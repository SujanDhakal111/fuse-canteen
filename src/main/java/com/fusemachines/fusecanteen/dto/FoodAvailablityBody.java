package com.fusemachines.fusecanteen.dto;

public class FoodAvailablityBody {
    public Long foodId ;
    public boolean isAvailableToday;

    FoodAvailablityBody(Long id, boolean isAvailableToday){
        this.isAvailableToday = isAvailableToday;
        this.foodId = id;

    }

    FoodAvailablityBody(){
    }

}
