package com.fusemachines.fusecanteen.dto;

import com.fusemachines.fusecanteen.Enum.OrderStatus;
import com.fusemachines.fusecanteen.model.Order;

public class UpdateOrderStatusDto {
    private Long orderId;
    private OrderStatus orderStatus;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = OrderStatus.valueOf(orderStatus);
    }

}
