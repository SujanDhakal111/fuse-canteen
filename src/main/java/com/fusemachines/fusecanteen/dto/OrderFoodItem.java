package com.fusemachines.fusecanteen.dto;

import com.fusemachines.fusecanteen.model.Food;
import com.fusemachines.fusecanteen.model.Order;

public class OrderFoodItem {
    private Food food;
    private Order order;

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
