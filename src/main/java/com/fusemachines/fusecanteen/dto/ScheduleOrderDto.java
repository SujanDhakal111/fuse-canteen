package com.fusemachines.fusecanteen.dto;

public class ScheduleOrderDto {
    private Long orderId;
    private String time;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
